export PATH := code/bin:$(PATH)
FAIDX := data/raw/genomes/reference/kordofan-giraffe_assembly.fa.fai
RM_OUTDIR := results/repeatmasker/repbase-cetartiodactyla+kordofan-giraffe
FILTER_BED := results/repeatmasker/repbase-cetartiodactyla+kordofan-giraffe/kordofan-giraffe_assembly.fa.out.300bp-slop.merged.complement.bed 

clean:
	$(RM) results/melt/genotypes_with_phylostrings.tsv results/melt/genotypes_with_phylostrings_species.tsv

splitfasta: data/raw/consensi/BovSINEs.fasta
	mkdir -p data/clean/consensi/$(notdir $^).split
	sed -e 's/\s\+/_/g' $^ > data/clean/consensi/$(notdir $^)
	seqkit split -O data/clean/consensi/$(notdir $^).split --by-id data/clean/consensi/$(notdir $^)

%.bed: %.gff
	PATH="$$PATH:/opt/bedops/2.4.35/bin" convert2bed --format=gff < $^ > $@

$(RM_OUTDIR)/kordofan-giraffe_assembly.fa.out.300bp-slop.bed: $(RM_OUTDIR)/kordofan-giraffe_assembly.fa.out.bed
	bedtools slop -b 300  -g $(FAIDX) -i $^ | bedtools sort -faidx $(FAIDX) > $@

$(RM_OUTDIR)/kordofan-giraffe_assembly.fa.out.300bp-slop.merged.bed: $(RM_OUTDIR)/kordofan-giraffe_assembly.fa.out.300bp-slop.bed
	bedtools merge -i $^ > $@

$(RM_OUTDIR)/kordofan-giraffe_assembly.fa.out.300bp-slop.merged.complement.bed: $(RM_OUTDIR)/kordofan-giraffe_assembly.fa.out.300bp-slop.merged.bed
	bedtools complement -g $(FAIDX) -i $^ > $@

%.300bp-slop.bed: %.out.bed
	bedtools slop -b 300 -g $(FAIDX) -i $^ | bedtools sort -faidx $(FAIDX) | bedtools merge - > $@

%.filtered.vcf: %.vcf $(FILTER_BED)
	vcftools --exclude-bed $(FILTER_BED) --vcf $^ --out $(basename $@) --recode

.PHONY: filter-stages.clean.pdf
filter-stages.clean.pdf: filter-stages.tex
	sed -e '/\\addlinespace/d' $^ > filter-stages.clean.tex
	pdflatex filter-stages.clean.tex
	pdflatex filter-stages.clean.tex
