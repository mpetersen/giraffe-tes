TE discovery in low-coverage giraffe genomes
============================================

Giraffes are a relatively young group of mammals with [four recognized
species](https://www.sciencedirect.com/science/article/pii/S0960982216307874)
that diverged [around 28
Mya](https://www.sciencedirect.com/science/article/pii/S0960982216315202):
northern giraffe (G. camelopardalis), southern giraffe (G. giraffa),
reticulated giraffe (G. reticulata), and Masai giraffe (G. tippelskirchi). The
genome of one giraffe species has been [analysed to find clues to its unique
morphology](https://www.nature.com/articles/ncomms11519), however, no attention
has been paid to mobile genetic elements (MEIs), also termed transposable
elements (TEs).

We used the kordofan giraffe genome assembly as a reference to map *de-novo*
sequenced low-coverage genomic data using BWA MEM (TODO cite). To identify TE
insertions based on discordant reads, we used the [Mobile Element Locator Tool
(MELT)](http://melt.igs.umaryland.edu) to identify TE insertions in the giraffe
genomes.

Directory structure:

code: scripts and programs used in the analysis. Especially important are the run-MELT-phase-[1-4] as well as the run-MELT-pipeline-complete scripts. Also the PCA.R script that computes PCAs.
data:
	clean: cleaned data ready for analysis. This comprises mainly the TE consensus sequences used for MELT.
	raw: 
		astral.tree: tree computed by Raphael with ASTRAL
		consensi: raw TE consensus sequences, with unclean headers and possible ambiguities
		genetic_distance.tree: giraffe tree computed by Raphael using ML and genetic distances
		genomes:
			reference: the Kordofan giraffe reference genome: Assembly (Fasta format, with index) and RepeatMasker annotation (BED format)
			samples: mapped reads from each of the giraffe individuals in BAM format, along with indices. Here MELT creates additional BAM files with the split and discordant reads.
doc:
	giraffe_genomes.xlsx: Statistics table from Raphael. This is read by study.Rmd and contains important data on the mapped reads.
	commands.log: the commands that Raphael used to repeat mask the Kordofan giraffe reference genome. It is a standard RepeatModeler/Masker procedure.
results:
	2019-05-16_L1_RTE_repeatmasker: quick RepeatMasker annotation with L1 and RTE consensus sequences I got from Vladimir
	2019-07-16_phobos: the start of a Phobos analysis which ran for weeks and never completed because of the power cut in August (?) 2019
	2019-07-17_N2bed: BED file with the coordinates of stretches of N
	2019-08-23_VCF2GDS: GDS formatted TE insertions. These files are being read by the PCA.R script.
	2019-08-27_filter-bed: results of the filter stages. These files are read by the filter-stages.Rmd document. Contains BED files with the genome assembly features used for filtering as well as filtered VCF files as the results.
	2019-09-02_q-indices: results of the "Q-indices" I had to compute for Vladimir at some point
	2019-09-18_clipped-reads-for-terminal-insertions: read subsets that cover terminal insertions; another dataset I had to pull for Vladimir
	alignments: Alignments. Only a single one and i forgot what for
	breakdancer: partial results of our experiment to use BreakDancer that was eventually cancelled by Maria :)
	figures: Pretty Figures!!
	hmmsearch: results from searching the ORFs for the reverse transcriptase. This used the inferred ORFs in the 'orfs' results directory
	melt: MELT analysis, the primary results of this study. these results are further subdivided by TE type
		line: L1 and RTE
		sine: BovTA2 and BOVA2
		genotypes_with_phylostrings.tsv: Tabular summary of what insertion is shared by what individuals. Also contains information on the number of individuals with that insertion by clade and the MRCA node numbers
		heterozygosity-*: data on the heterozygosity ratio
		insertions-passing-all-filters-by-species.tsv: Summary table listing just the number of detected insertions that passed all implemented filters.
	orfs: ORFs inferred by getorf(1). I also shortened the headers and created a dictionary for the shortened headers
	pictures: Mainly giraffe photos for the final tree
	repeatmasker: repeatmasker annotation with both the Cetartiodactyla Repbase library and Cetartiodactyla + RepeatModeler library
	repeatmodeler: RepeatModeler annotation and species-specific repeat library
	smoove: short dab into trying to use SMOOVE, which outputs super complex VCF files
study.Rmd: RMarkdown document detailing the study that contains all statistical analyses and the code to generate all figures.

Please note that the population genetics analysis input files are part of the Dryad repository for this study (doi:10.5061/dryad.ksn02v74f).
