#!/usr/bin/awk

# Compute nt coordinates from aa coordinates in a BED file.

BEGIN { OFS = "\t" }

{
	$2 = $2 * 3
	$3 = $3 * 3
	print
}
