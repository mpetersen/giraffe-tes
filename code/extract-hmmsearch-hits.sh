#!/bin/bash

# Copyright 2018, Malte Petersen <mptrsen@uni-bonn.de>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.


set -e
set -o pipefail nounset

if [[ $# -ne 3 ]]; then
	echo "Usage: $0 assembly_file orf_file hmmsearch_domtblout_file"
	exit 1
fi

assembly=$1
dictionary=$2
domtbl=$3

# make table from dictionary -> table.txt
# this could also just be created directly from the ORF headers so we will not need the dict
#perl code/make-table-from-dict.pl $dictionary > $dictionary.table.txt

# import table to sqlite database -> sqlite
# omit this for now, the db is not used yet (but it should be because it saves memory -- a lot)
#bash code/import-dict-to-sqlite.sh $dictionary.table.txt

# convert hmmsearch hit domtbl output to BED format -> BED
bash code/hmmsearch-domtbl2bed.sh $hmmsearch_domtblout_file > $hmmsearch_domtblout_file.bed

# convert hit aa coordinates to nt coordinates -> BED
awk -f code/aa-coords2nt-coords.awk $hmmsearch_domtblout_file.bed > $hmmsearch_domtblout_file.nt.bed

# make sure we have a dictionary
if [[ ! -s $orf_file.dict ]]; then
# no dictionary, create it
	echo "## No dictionary found for $orf_file, creating..."
	awk '/^>/ { sub("^>", ""); sub(" ", "\t"); print }' $orf_file > $orf_file.dict
fi

## recalculate nt coordinates to the orf positions (using the database), create a BED file
### this is currently done with a perl script,
perl code/nt-coords-on-orf2nt-coords-on-scaffold.pl \
	$hmmsearch_domtblout_file.nt.bed \
	$orf_file \
	> $hmmsearch_domtblout_file.nt.on-scaffold.bed
# TODO refactor to use the database and make it a bash script
#bash code/orf-coords2scaffold-coords.sh $hmmsearch_domtblout_file.nt.bed > $hmmsearch_domtblout_file.nt.on-scaffold.bed

# extract sequences from assembly with bedtools getfasta -> fasta
# use -s to make it strand-aware if that information is in the BED file (should be)
bedtools getfasta \
	-s 
	-fi $assembly 
	-bed $hmmsearch_domtblout_file.nt.on-scaffold.bed 
	-fo $hmmsearch_domtblout_file.nt.on-scaffold.fa

exit # so I can leave the following here

# Can also add flanks with `bedtools slop`:
bedtools slop \
	-b 7000 \
	-g kordofan-giraffe_assembly.fa.fai \
	-i orfs.domtbl.nt.bed.on-scaffolds.1100+.bed \
	| bedtools getfasta \
		-fi kordofan-giraffe_assembly.fa \
		-fo orfs.domtbl.nt.bed.on-scaffolds.1100+.fa \
		-bed - \
		> output.fa
