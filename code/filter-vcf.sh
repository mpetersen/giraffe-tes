#!/bin/bash

# Copyright 2019, Malte Petersen <mptrsen@uni-bonn.de>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e
set -o nounset
set -o pipefail

if [[ $# -ne 2 ]]; then
	echo "Usage: $0 bedfile vcffile"
	exit 1
fi

bedfile="$1"
vcffile="$2"
outdir="results/2019-08-27_filter-bed/filtered-vcfs/$(basename $bedfile .bed)"

echo "## Creating $outdir"
mkdir -p "$outdir"

outfile_basename=$outdir/$(basename $vcffile .vcf).filtered
vcftools --exclude-bed $bedfile --vcf $vcffile --out $outfile_basename --recode --recode-INFO-all

