#!/usr/bin/env python3

# fix the BreakDancer config file: as it is, the "lib" names are often
# identical, and this causes breakdancer to skip redundant "lib"s. This script
# renames the libraries to include the sample ID, thus making them less
# ambiguous.

import sys
import fileinput
import re

for line in fileinput.input():
    fields = line.strip().split("\t")
    keys = [ field.split(":")[0] for field in fields ]
    kv = { f.split(":")[0] : f.split(":")[1] for f in fields }
    matches = re.search("([a-zA-Z0-9-]+)_dupmarked", kv["map"])
    kv["lib"] = matches.group(1) + "-" + kv["lib"]
    l = [ k + ":" + kv[k] for k in keys ]
    print("\t".join(l))
