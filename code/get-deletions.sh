#!/bin/bash

# extract fasta sequences for each of the samples listed in a VCF file. The
# following was the request:

# extract all BOVA2 copies from the 8 individuals that we screened with the
# MELT deletion. It would be good to have them in one file per individual.

# Currently the script only works from the results/melt/deletion directory. Run
# it from there or adapt accordingly.

set -e
set -o pipefail
set -o nounset

if [[ $# -ne 2 ]]; then
	echo "Usage: $0 assembly vcffile"
	exit 1
fi

# Make tab-separated file with zero-based coordinates for each SV
bcftools query -f'[%CHROM\t%POS0\t%END\t%INFO/SVTYPE\t%SAMPLE\t%GT\n]'  DEL.final_comp.vcf \
| grep  'BOV-A2' \
| grep -v '0/0' \
> BOV-A2.deletions.bedish

# Make separate TSV file for each sample
for f in *.tsv; do
	s=${f%.del.tsv}
	grep "$s" BOV-A2.deletions.bedish > $s.BOV-A2.del.tsv
done

# Extract the sequences from the reference assembly, put into separate file for each sample
for f in *.BOV-A2.del.bed; do
	bedtools getfasta \
		-name+ \
		-fi ../../../data/raw/genomes/reference/kordofan-giraffe_assembly.fa \
		-fo $f.fa \
		-bed $f
done
