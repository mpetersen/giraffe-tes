#!/bin/bash

# Convert a repeatmasker GFF file to BED format.
# This removes the 'Target "Motif:' part as well as 
# the coordinates on the reference consensus sequence,
# and reorders the columns appropriately.

set -e
set -o pipefail
set -o nounset

export PATH=$PATH:/opt/bedops/2.4.35/bin

if [[ $# -ne 1 ]]; then
	echo "Usage: $0 input_gff_file"
	exit 1
fi

infile="$1"

convert2bed-typical -o bed -i gff < "$infile" \
	| awk -F "\t" 'BEGIN { OFS="\t"}
		{	sub("Target \"Motif:", "", $10);
			sub("\".+$", "", $10);
			print $1, $2, $3, $10, $5, $6
		}'
