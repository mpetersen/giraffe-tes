#!/bin/bash

# Convert a tabular output from hmmsearch --domtblout to BED format.

if [[ $# -ne 1 ]]; then
	echo "Usage: $0 output.domtbl"
	exit 1
fi

grep -v '^#' $1 \
	| tr -s ' ' '\t' \
	| awk 'BEGIN { OFS = "\t" } { print $1, $18-1, $19-1, $3, $8, "." }'
