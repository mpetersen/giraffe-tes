#!/bin/bash

# Copyright 2018, Malte Petersen <mptrsen@uni-bonn.de>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e
set -o pipefail nounset

if [[ $# -ne 1 ]]; then
	echo "Usage: $0 dictionary_table"
	exit 1
fi

dict="$1"

echo "# Creating orf table..."

sqlite3 $dict.sqlite 'CREATE TABLE coords (orf CHAR, scaffold CHAR, num_orf INT, start INT, end INT);'

echo "# Table created."

echo "# Importing dictionary table..."
sqlite3 $dict.sqlite -import "$dict.table.txt" coords

echo "# Done. Bye."
