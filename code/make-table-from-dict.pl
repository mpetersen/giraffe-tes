#!/usr/bin/perl
use strict;
use warnings;
use autodie;

my $dictfile = shift @ARGV or die;

my $dict = &convert_dictionary($dictfile);

sub convert_dictionary {
	my $f = shift @_;
	my $r = { };
	open my $fh, '<', $f;
	while (my $l = <$fh>) {
		chomp $l;
		my ($orf, $rest) = split /\t/, $l;
		$rest =~ /^\[(\d+) - (\d+)\]/;
		my $from = $1;
		my $to   = $2;
		$orf =~ /^(.+)_(\d+)$/;
		my $scaffold = $1;
		my $orf_number = $2;
		printf "%s\t%s\t%d\t%d\t%d\n", $orf, $scaffold, $orf_number, $from, $to;
	}
}

