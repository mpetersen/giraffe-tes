try {
	// test whether reference and MELT.jar are present
	file(reference).exists()
	file(MELT_jar).exists()
}
catch(Exception e) {
	println("Required parameters: reference, MELT_jar")
	return
}

if (!javaMemory) { def javaMemory = 100 }

prepare = {
	branch.bam_index = input + '.bai'
	branch.reference_index = reference + '.fai'
	multi "ln -sf $input.bam",
	"ln -sf $bam_index",
	"ln -sf $reference",
	"ln -sf $reference_index",
	"echo 'Input: $input'"
	forward file(input).getName()
}

// Preprocess the sample BAM
Preprocess = {
	exec """
		echo 'Running Preprocess on $input'
		
		echo java -Xmx${javaMemory}G -jar ${MELT_jar} Preprocess -h kordofan-giraffe_assembly.fa -bamfile $input
	"""
}

IndivAnalysis = {
	exec """
	echo 'Running IndivAnalysis'

	echo java  -Xmx${javaMemory}G -jar ${MELT_jar} IndivAnalysis -a -bamfile $input -c 21 -h $reference -t BOVA2_MELT.zip -w .
	"""
}

GroupAnalysis = {
	exec "echo 'Running GroupAnalysis'"
}

Genotype =  {
	exec "echo 'Running Genotype'"
}

MakeVCF = {
	exec "echo 'Running MakeVCF'"
}

run { "%.bam" * [ prepare + Preprocess + IndivAnalysis ] + GroupAnalysis + "%.bam" * [ Genotype ] + MakeVCF }
