#!/usr/bin/perl
use strict;
use warnings;
use autodie;

use File::Spec::Functions;
use Data::Dumper;

my $usage = "Usage: $0 bedfile orffile\n";

die $usage unless $#ARGV != 2; # need two arguments!

my $bedfile = shift @ARGV or die $usage;
my $orffile = shift @ARGV or die $usage;

my $dictfile = catfile($orffile . '.dict');

unless (-f $dictfile) {
	die <<EOT;
No ORF dictionary found. Create one with this command:

	awk '/^>/ { sub("^>", ""); sub(" ", "\\t"); print }' $orffile > $orffile.dict
EOT
}

print "## Reading dictionary\n";

# fill this with the dictionary first
my $orfs_on_scaffolds = &slurp_dictionary($dictfile);  # this will be large.

sub slurp_dictionary {
	my $f = shift @_;
	my $r = { };
	open my $fh, '<', $f;
	while (my $l = <$fh>) {
		chomp $l;
		my ($orf, $rest) = split /\t/, $l;
		$rest =~ /^\[(\d+) - (\d+)\]/;
		my $revsense = $1 > $2 ? 1 : 0;
		if ($revsense) {
			$r->{$orf}->{'from'} = $2;
			$r->{$orf}->{'to'}   = $1;
			$r->{$orf}->{'strand'} = '-';
		}
		else {
			$r->{$orf}->{'from'} = $1;
			$r->{$orf}->{'to'}   = $2;
			$r->{$orf}->{'strand'} = '+';
		}
		$orf =~ /^(.+)_(\d+)$/;
		$r->{$orf}->{'scaffold'} = $1;
		$r->{$orf}->{'orf_number'} = $2;
	}
	return $r;
}

print "## Done.\n";

print "## Reading BED file\n";

my $outfile = catfile($bedfile . ".on-scaffolds.bed");
open my $outfh, '>', $outfile;

open my $fh, '<', $bedfile;
while (<$fh>) {
	chomp;
	my @fields = split /\t/;
	my $orf = $fields[0];
	my $bed_start = $fields[1];
	my $bed_end = $fields[2];
	(my $scaffold = $orf) =~ s/_[^_]+$//;
	next unless defined $orfs_on_scaffolds->{$orf};
# adjust ORF coordinates with coords on scaffold
	my $orf_start = $orfs_on_scaffolds->{$orf}->{'from'};
	my $orf_end   = $orfs_on_scaffolds->{$orf}->{'to'};
	my $score = '.';
	my $strand = $orfs_on_scaffolds->{$orf}->{'strand'};
	my $bed_start_on_scaffold = $orf_start + $bed_start - 1; # 0-based
	my $bed_end_on_scaffold = $bed_start_on_scaffold + ($bed_end - $bed_start); # only add the length, stupid
# print BED line :
	printf $outfh "%s\t%d\t%d\t%s:%d-%d\t%s\t%s\n", $scaffold, $bed_start_on_scaffold, $bed_end_on_scaffold, $orf, $orf_start, $orf_end, $score, $strand;
}
close $fh;
close $outfh;

print "## All done. Bye.\n";
exit;

package Seqload::Fasta;

# Documentation before the code
=head1 NAME

Seqload::Fasta

=head1 DESCRIPTION

A library for handling FASTA sequences in an object-oriented fashion. 
Incompatibility with BioPerl is intentional.

=head1 SYNOPSIS

	use Seqload::Fasta qw(fasta2csv check_if_fasta);
	
	# test whether this is a valid fasta file
	check_if_fasta($filename) or die "Not a valid fasta file: $filename\n";

	# open the file, return fasta file object
	my $file = Seqload::Fasta->open($filename);
	
	# loop through the sequences
	while (my ($hdr, $seq) = $file->next_seq) {
	  print $hdr . "\n" . $seq . "\n";
	}

	# just undef the object, the destructor closes the file
	undef($file)

	# convert a fasta file to a csv file
	fasta2csv($fastafile, $csvfile);


=head1 METHODS

=head2 open(FILENAME)

Opens a fasta file. Returns a sequence database object.

=head2 next_seq

Returns the next sequence in a sequence database object as an array (HEADER,
SEQUENCE). Note that the '>' character is truncated from the header.

	($header, $sequence) = $file->next_seq;

=head1 FUNCTIONS

=head2 fasta2csv($fastafile, $csvfile)

Converts a fasta file into a csv file where each line consists of
'HEADER,SEQUENCE'. Manages opening, parsing and closing of the files, no
additional file handles necessary.

=head2 check_if_fasta($file)

Checks whether or not the specified file is a valid fasta file (i.e., starts with a header line). Returns 0 if not and 1 otherwise.

=cut

use strict;
use warnings;
use Carp;
require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw( fasta2csv check_if_fasta );

# Constructor. Returns a sequence database object.
sub open {
	my ($class, $filename) = @_;
	open (my $fh, '<', $filename)
		or confess "Fatal: Could not open $filename\: $!\n";
	my $self = {
		'filename' => $filename,
		'fh'       => $fh
	};
	bless($self, $class);
	return $self;
}

# Returns the next sequence as an array (hdr, seq). 
# Useful for looping through a seq database.
sub next_seq {
	my $self = shift;
	my $fh = $self->{'fh'};
	# this is the trick that makes this work
	local $/ = "\n>"; # change the line separator
	return unless defined(my $item = readline($fh));  # read the line(s)
	chomp $item;
	
	if ($. == 1 and $item !~ /^>/) {  # first line is not a header
		croak "Fatal: " . $self->{'filename'} . " is not a FASTA file: Missing descriptor line\n";
	}

	# remove the '>'
	$item =~ s/^>//;

	# split to a maximum of two items (header, sequence)
	my ($hdr, $seq) = split(/\n/, $item, 2);
	$hdr =~ s/\s+$//;	# remove all trailing whitespace
	$seq =~ s/>//g if defined $seq;
	$seq =~ s/\s+//g if defined $seq; # remove all whitespace, including newlines

	return($hdr, $seq);
}

# Closes the file and undefs the database object.
sub close {
	my $self = shift;
	my $fh = $self->{'fh'};
	my $filename = $self->{'filename'};
	close($fh) or carp("Warning: Could not close $filename\: $!\n");
	undef($self);
}

# Destructor. This is called when you undef() an object
sub DESTROY {
	my $self = shift;
	$self->close;
}

