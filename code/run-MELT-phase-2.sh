#!/bin/bash

# Copyright 2018, Malte Petersen <mptrsen@uni-bonn.de>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.


# to be called like: 
#
# 	$0 parameterfile sample outputdir

set -e
set -o pipefail
set -o nounset

if [[ $# -ne 2 ]]; then
	echo "Usage: $0 parameter_table.tsv outdir"
	exit 1
fi

parameterfile="$1"
outdir="$2"

MELT_jar="/home/mpetersen/projects/giraffe-tes/code/MELTv2.1.5/MELT.jar"
java_memory=100G

# find any sample line in the parameter table and extract all parameters
mei=$(       grep -m 1 -v '^#' "$parameterfile" | cut -f2)
bamfile=$(   grep -m 1 -v '^#' "$parameterfile" | cut -f3)
reference=$( grep -m 1 -v '^#' "$parameterfile" | cut -f4)
meifile=$(   grep -m 1 -v '^#' "$parameterfile" | cut -f5)
readlength=$(grep -m 1 -v '^#' "$parameterfile" | cut -f6)
insertsize=$(grep -m 1 -v '^#' "$parameterfile" | cut -f7)
coverage=$(  grep -m 1 -v '^#' "$parameterfile" | cut -f8)

# Run GroupAnalysis

mkdir -p "$outdir/2-GroupAnalysis"
java -Xmx${java_memory} -jar "$MELT_jar" GroupAnalysis \
	-discoverydir "$outdir/1-IndivAnalysis" \
	-h "$reference" \
	-n "${reference}.annotation.bed" \
	-r $readlength \
	-t "$meifile" \
	-w "${outdir}/2-GroupAnalysis" \
| tee "${outdir}/GroupAnalysis.log"


echo
echo "## Done GroupAnalysis. Now run Genotype for all samples."
echo
