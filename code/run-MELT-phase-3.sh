#!/bin/bash

# Copyright 2018, Malte Petersen <mptrsen@uni-bonn.de>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.


# to be called like: 
#
# 	$0 parameterfile sample outputdir

set -e
set -o pipefail
set -o nounset

if [[ $# -ne 3 ]]; then
	echo "Usage: $0 parameter_table.tsv sample outdir"
	exit 1
fi

parameterfile="$1"
sample="$2"
outdir="$3"

MELT_jar="/home/mpetersen/projects/giraffe-tes/code/MELTv2.1.5/MELT.jar"
java_memory=100G

# find the sample line in the parameter table and extract all parameters
mei=$(       grep "\b$sample\b" "$parameterfile" | cut -f2)
bamfile=$(   grep "\b$sample\b" "$parameterfile" | cut -f3)
reference=$( grep "\b$sample\b" "$parameterfile" | cut -f4)
meifile=$(   grep "\b$sample\b" "$parameterfile" | cut -f5)
readlength=$(grep "\b$sample\b" "$parameterfile" | cut -f6)
insertsize=$(grep "\b$sample\b" "$parameterfile" | cut -f7)
coverage=$(  grep "\b$sample\b" "$parameterfile" | cut -f8)

# After GroupAnalysis, run Genotyping

mkdir -p "$outdir/3-Genotype"
java -Xmx${java_memory} -jar "$MELT_jar" Genotype \
	-h "$reference" \
	-p "$outdir/2-GroupAnalysis" \
	-w "$outdir/3-Genotype" \
	-t "$meifile" \
	-bamfile "$bamfile" \
	-e $insertsize \
| tee "${outdir}/${sample}.Genotype.log"

echo
echo "## Done genotyping for sample $sample"
echo
echo 'Now wait for all genotypings to finish and run MakeVCF'
echo
