#!/bin/bash

# Copyright 2018, Malte Petersen <mptrsen@uni-bonn.de>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Run the final stage of MELT, creating the VCF output file.
# 
# to be called like: 
#
# 	$0 parameterfile outputdir

set -e
set -o pipefail
set -o nounset

if [[ $# -ne 2 ]]; then
	echo "Usage: $0 parameter_table.tsv outdir"
	exit 1
fi

parameterfile="$1"
outdir="$2"

MELT_jar="/home/mpetersen/projects/giraffe-tes/code/MELTv2.1.5/MELT.jar"
java_memory=100G

# find any sample line in the parameter table and extract all parameters
mei=$(       grep -m 1 -v '^#' "$parameterfile" | cut -f2)
bamfile=$(   grep -m 1 -v '^#' "$parameterfile" | cut -f3)
reference=$( grep -m 1 -v '^#' "$parameterfile" | cut -f4)
meifile=$(   grep -m 1 -v '^#' "$parameterfile" | cut -f5)
readlength=$(grep -m 1 -v '^#' "$parameterfile" | cut -f6)
insertsize=$(grep -m 1 -v '^#' "$parameterfile" | cut -f7)
coverage=$(  grep -m 1 -v '^#' "$parameterfile" | cut -f8)

# After GroupAnalysis, run Genotyping

mkdir -p "$outdir/4-MakeVCF"
java -Xmx${java_memory} -jar "$MELT_jar" MakeVCF \
	-h "$reference" \
	-t "$meifile" \
	-p "$outdir/2-GroupAnalysis" \
	-genotypingdir "$outdir/3-Genotype" \
	-w "$outdir/4-MakeVCF" \
	-o "$outdir/4-MakeVCF" \
| tee "${outdir}/MakeVCF.log"

echo
echo "## Done making final VCF in '$outdir/4-MakeVCF'"

