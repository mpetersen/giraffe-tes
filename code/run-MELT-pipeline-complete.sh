#!/bin/bash

# Run MELT on a set of BAM mapping files

set -e
set -o pipefail
set -o nounset

if [[ $# -ne 2 ]]; then
	echo "Usage: $0 parameter_table.tsv outdir"
	exit 1
fi

codedir=$(dirname $0)
params="$1"
outdir="$2"

MELT_jar="/home/mpetersen/projects/giraffe-tes/code/MELTv2.1.5/MELT.jar"

function set_reference {
	reference=$(grep -v '^#' "$1" | cut -f3 | tail -n 1)
}

function set_readlength {
	readlength=$(grep -v '^#' "$1" | cut -f5 | tail -n 1)
}

function set_meifile {
	meifile=$(grep -v '^#' "$1" | cut -f4 | tail -n 1)
}


set_reference $params
set_readlength $params
set_meifile $params

echo "## Parameters"
echo "## - parameter table: $params"
echo "## - output directory: $outdir"
echo "## - reference: $reference"
echo "## - read length: $readlength"
echo "## - MEI file: $meifile"

# link everything
mkdir -p "$outdir"
echo "## linking input files..."
grep -v '^#' "$params" | while read sample bamfile reference mei readlength insertsize coverage; do
	set -o verbose # echo commands before executing
	ln --symbolic --relative --force "$bamfile" "$outdir"
	ln --symbolic --relative --force "$bamfile.bai" "$outdir"
	ln --symbolic --relative --force "$reference" "$outdir"
	ln --symbolic --relative --force "$reference.fai" "$outdir"
	ln --symbolic --relative --force "$reference.annotation.bed" "$outdir"
	ln --symbolic --relative --force "$mei" "$outdir"
	ln --symbolic --relative --force "$params" "$outdir"
	set +v # disable echoing again
done

# enter working dir
cd $outdir

# Preprocess
echo "## Preprocessing input files..."
grep -v '^#' $(basename $params) | while read sample bamfile reference meifile readlength insertsize coverage; do
	bash $codedir/run-MELT-preprocess.sh $(basename $reference) $(basename $bamfile)
done

echo "## Waiting for all subprocesses to finish"
wait
echo "## All subprocesses finished"

# IndivAnalysis
echo "## Running IndivAnalysis..."
grep -v '^#' $(basename $params) | while read sample bamfile reference meifile readlength insertsize coverage; do
	java  -Xmx100G -jar $MELT_jar IndivAnalysis -a -bamfile $(basename $bamfile) -c $coverage -h $(basename $reference) -t $(basename $meifile) -w . -bowtie "/opt/bowtie2-2.3.4.3-linux-x86_64/bowtie2"
done

echo "## Waiting for all subprocesses to finish"
wait
echo "## All subprocesses finished"

# GroupAnalysis
echo "Running GroupAnalysis..."
set_reference $(basename $params)
set_readlength $(basename $params)
set_meifile $(basename $params)
java  -Xmx100G -jar $MELT_jar GroupAnalysis -discoverydir . -h $(basename $reference) -n $(basename $reference).annotation.bed -r $readlength -t $(basename $meifile) -w .


# Genotype
echo "## Running Genotype"
grep -v '^#' $(basename $params) | while read sample bamfile reference meifile readlength insertsize coverage; do
	java  -Xmx100G -jar $MELT_jar Genotype  -h $(basename $reference) -p . -w . -t $(basename $meifile) -bamfile $(basename $bamfile) -e $insertsize
done

echo "## Waiting for all subprocesses to finish"
wait
echo "## All subprocesses finished"


# MakeVCF
echo "## Making final VCF..."
set_reference $(basename $params)
set_meifile $(basename $params)
java -Xmx100G -jar $MELT_jar  MakeVCF -t $(basename $meifile) -h $(basename $reference) -genotypingdir . -w . -p .


# All done
echo "## All done."
