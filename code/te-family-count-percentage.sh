#!/bin/bash

# Compute counts and genome coverage for each TE type listed in a
# RepeatMasker output table (.out file). Takes three arguments: 
# - genome assembly index (to calculate the size)
# - .out file
# - output file (the results are written to this)

if [[ $# -ne 3 ]]; then
	echo "Usage: $0 genome-assembly.fa.fai repeatmasker.out outputfile.txt" > /dev/stderr
	exit 1
fi

echo "## Parameters:" > /dev/stderr
echo "## - Genome assembly index: $1" > /dev/stderr
echo "## - RepeatMasker output table: $2" > /dev/stderr
echo "## - Output file: $3" > /dev/stderr

outputfile="$3"

echo "## Calculating genome size..." > /dev/stderr

# This AWK script just sums up the sequence lengths and returns the total
genomesize=$(awk '{ s += $2 } END { print s }' "$1")

echo "## Done. Genome size: $genomesize bp" > /dev/stderr

echo "## Calculating TE percentages..." > /dev/stderr

# This AWK script reads the .out file, counts the copies, and computes the percentage.
read -r -d '' awk_script <<-'__END_OF_SCRIPT__'
{
	elem = $11 "#" $10;
	len[elem] += ($7 - $6) + 1;
	count[elem] += 1;
}
END {
	for (elem in count) {
		split(elem, fields, "#");
		percentage = (len[elem] * 100) / genomesize;
		printf("%s\t%s\t%d\t%f\n", fields[1], fields[2], count[elem], percentage);
	}
}
__END_OF_SCRIPT__

# Make a nice sorted table.
(printf "%s\t%s\t%s\t%s\n" superfamily family count percentage 
tail -n +4 "$2" \
	| awk -v genomesize=$genomesize "$awk_script" \
	| sort
) \
| column -t \
> "$outputfile"

echo "## Done. Output file: '$outputfile'" > /dev/stderr

echo "## All done. Bye." > /dev/stderr
