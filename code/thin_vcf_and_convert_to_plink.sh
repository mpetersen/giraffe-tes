#!/bin/bash

set -euo pipefail

if [[ $# -ne 1 ]]; then
	echo "Usage: $0 snps.filtered.bcf"
	exit 1
else
	in_bcf="$1"
fi

# thin vcf file (1 snp per 40Kb):
bcftools view -O z ${in_bcf} -o ${in_bcf%.bcf}.vcf.gz
vcftools --thin 40000 --gzvcf ${in_bcf%.bcf}.vcf.gz -c --recode --recode-INFO-all | gzip > mysnps.thinned.40000.vcf.gz

# convert to plink format:
vcftools --gzvcf mysnps.thinned.40000.vcf.gz --plink --out mysnps.thinned.40000
# add positional information to map-file:
cut -f2 mysnps.thinned.40000.map | cut -f1 -d ':' > mycontigs.txt \
&& cut -f2,3,4 mysnps.thinned.40000.map > mymap.txt \
&& paste mycontigs.txt mymap.txt > mysnps.thinned.40000.map \
&& rm mycontigs.txt mymap.txt

# Maria excluded from TE dataset (i.e., 'dm0row_diploid.tsv') the samples MF24 and ENP11 (from 50 down to 48 individuals)
# For consistency, I also excluded those from the SNP-data:
grep -v 'MF24' mysnps.thinned.40000.ped | grep -v 'ENP11' | sed 's/-/_/g' > mysnps.thinned.40000.48inds.ped
cp mysnps.thinned.40000.map mysnps.thinned.40000.48inds.map 

# Lastly, convert to binary PLINK files:
plink --file mysnps.thinned.40000.48inds --allow-extra-chr --chr-set 95 --make-bed --recode A --out mysnps.thinned.40000

# Clean up:
rm mysnps.thinned.40000.map mysnps.thinned.40000.48inds.*
