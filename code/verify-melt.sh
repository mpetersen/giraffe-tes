#!/bin/bash
set -e
set -o nounset
set -o pipefail

infile=$1

id=${infile%_dupmarked.bam.disc}
id=$(basename $id)
echo $id

extracted_reads=scratch/L1.insertion1.${id}.sam

echo "# extracting discordant reads"
samtools view -h -L scratch/L1.insertion1.bed $infile > $extracted_reads

echo "# getting read ids"
grep -v '^@' $extracted_reads | cut -f1 > $extracted_reads.reads.txt

mapped_to_L1=scratch/L1.insertion1.${id}.mapped-to-L1.sam

echo "# finding reads mapped to L1"
grep --fixed-strings -f scratch/L1.insertion1.${id}.sam.reads.txt <(samtools view results/melt/line/L1/1-IndivAnalysis/${id}_dupmarked.L1.aligned.final.sorted.bam) > $mapped_to_L1

echo "# $(lc $mapped_to_L1) reads for $id"
