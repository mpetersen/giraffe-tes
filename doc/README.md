giraffe_genomes.xlsx
--------------------

Information about the BAM files that I received from Raphael Coimbra (2019-04-05).

commands.log
------------

Lists the commands that Raphael used to run RepeatModeler and RepeatMasker on
the giraffe reference genome.
