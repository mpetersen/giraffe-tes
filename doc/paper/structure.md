---
title: Differential activity of transposable elements in giraffe populations supports subspecies hypothesis
author: 
	- Malte Petersen
	- Vladimir Kapitonov
	- Maria Nilsson-Janke
date: 2019-06-28
---

Outline.

# Introduction

- Giraffe separated into four species with subspecies, based on mitochondrial
	and nuclear markers
- Population genetics suggests clear clustering
- Giraffe genome analysed & published:
	- Focus on neck morphology, cardiovascular system, specialised genes for diet
	- Transposable elements entirely neglected in analysis
- We investigated the activity of LINEs (L1, RTE) and SINEs (BOVA2, tA2) in 50
	giraffe genomes:
	- Find congruence with clustering by species
	- Find conflicting phylogenetic signal?

# Material and methods

- 50 low-coverage giraffe genomes
- 1 Kordofan giraffe reference genome assembly
- MELT
- Samtools
- Bedtools
- Bcftools
- R
	- tidyverse
	- vcfR
- GNU parallel
- ts

# Results

Figures:

- Tree with insertions
- Insertion length distribution
- Repeat landscape for L1, RTE, BOVA2, tA2
- Shared insertions a la Lammers et al. (2019)
- More figures/supplement:
	- Venn diagram of shared insertions?
	- Shared insertions by insertion (Gardner et al., Fig 2B)?
	- ?
	- ?
	- ?

Tables:

- Insertions per TE type per clade/population/individual

- Repeat landscape shows recent activity of L1, RTE
- Insertion numbers show that there is differential TE activity in the four
	giraffe species and subspecies.
- Median number of insertions nicely fits to the tree
- Distribution of insertions along internal branches incongruent with topology
- Heterozygous vs homozygous ratio 2:1

# Discussion

We find that TE insertion abundance reflects the clustering into four species
and their subspecies, however TE insertion patterns appear to be incongruent
with bifurcating giraffe evolution.

Tree might be wrong; MELT error rate of about 10%; insertion rate not equal to
fixation rate: reflected in difference between heterozygous and homozygous
insertions

We adressed the analyses on population level instead of on individual level for
these reasons.

Our results show that TE activity is lineage-specific in giraffe and nicely
traces the pattern of giraffe speciation, albeit on population level



# Acknowledgments

- Raphael Coimbra, Sven Winter, and Axel Janke for providing the giraffe data
- Fritjof Lammers for help running MELT and supporting scripts for analysis
- Senckenberg TBG
